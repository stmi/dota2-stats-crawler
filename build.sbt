ThisBuild / name := "dota2-stats-crawler"
ThisBuild / version := "1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.2"
ThisBuild / organization := "pl.stmi"

lazy val dota2_stats_crawler = (project in file("."))
  .settings(
    assemblyJarName in assembly := "dota2-stats-crawler.jar",
    mainClass in assembly := Some("pl.stmi.dota2.crawler.main.CLIMain")
  )
